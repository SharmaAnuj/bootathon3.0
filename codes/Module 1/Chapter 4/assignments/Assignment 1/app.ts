var add_n1 : HTMLInputElement = <HTMLInputElement>document.getElementById('add_n1');
var add_n2 : HTMLInputElement = <HTMLInputElement>document.getElementById('add_n2');
var add_ans : HTMLInputElement = <HTMLInputElement>document.getElementById('add_ans');

var subs_n1 : HTMLInputElement = <HTMLInputElement>document.getElementById('subs_n1');
var subs_n2 : HTMLInputElement = <HTMLInputElement>document.getElementById('subs_n2');
var subs_ans : HTMLInputElement = <HTMLInputElement>document.getElementById('subs_ans');

var mult_n1 : HTMLInputElement = <HTMLInputElement>document.getElementById('mult_n1');
var mult_n2 : HTMLInputElement = <HTMLInputElement>document.getElementById('mult_n2');
var mult_ans : HTMLInputElement = <HTMLInputElement>document.getElementById('mult_ans');

var divi_n1 : HTMLInputElement = <HTMLInputElement>document.getElementById('divi_n1');
var divi_n2 : HTMLInputElement = <HTMLInputElement>document.getElementById('divi_n2');
var divi_ans : HTMLInputElement = <HTMLInputElement>document.getElementById('divi_ans');

function add() {
    var ans: number = parseFloat(add_n1.value) + parseFloat(add_n2.value);
    add_ans.value = ans.toString();
    console.log(ans);
}


function subs() {
    var ans: number = parseFloat(subs_n1.value) - parseFloat(subs_n2.value);
    subs_ans.value = ans.toString();
    console.log(ans);
}


function mult() {
    var ans: number = parseFloat(mult_n1.value) * parseFloat(mult_n2.value);
    mult_ans.value = ans.toString();
    console.log(ans);
}


function divi() {
    var ans: number = parseFloat(divi_n1.value) / parseFloat(divi_n2.value);
    divi_ans.value = ans.toString();
    console.log(ans);
}