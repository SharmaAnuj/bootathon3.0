function area()
{
    var a :HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var b :HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var c :HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    var d :HTMLInputElement = <HTMLInputElement>document.getElementById("t4");
    var e :HTMLInputElement = <HTMLInputElement>document.getElementById("t5");
    var f :HTMLInputElement = <HTMLInputElement>document.getElementById("t6");
    var g :HTMLInputElement = <HTMLInputElement>document.getElementById("t7");
    var h :HTMLInputElement = <HTMLInputElement>document.getElementById("t8");
    
    //typecasting to float
    var x1:number = +a.value;
    var y1 :number= +b.value;
    var x2 :number= +c.value;
    var y2 :number= +d.value;
    var x3 :number= +e.value;
    var y3 :number= +f.value;
    var x4 :number= +g.value;
    var y4 :number= +h.value;

    //Input validation
    if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(x4) || isNaN(y1) || isNaN(y2) || isNaN(y3) || isNaN(y4))
    {   
        alert("Enter a valid number");
    }
    else
    {

        var area = (x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2;

        var pab = (x4*(y1-y2)+x1*(y2-y4)+x2*(y4-y1))/2;

        var pbc = (x4*(y2-y3)+x2*(y3-y4)+x3*(y4-y2))/2;

        var pac = (x4*(y1-y3)+x1*(y3-y4)+x3*(y4-y1))/2;

        var sum = Math.abs(pab)+Math.abs(pbc)+Math.abs(pac);

        if((pab === 0 || pbc === 0 || pac === 0) && (Math.abs(area)-sum) === 0)
        {
            document.getElementById("display").innerHTML = "<br><b>Congrats!!! Point lies on triangle</b>";
        }
        else
        {
            // inside the triangle
            if((Math.abs(area)-sum) < 0.000001 && (Math.abs(area)-sum) >= 0)
            {
                document.getElementById("display").innerHTML = "<br><b>Point lies inside triangle</b>";
            }
            // outside the triangle
            else
            {
                document.getElementById("display").innerHTML = "<br><b>Point lies outside triangle</b>";
            }
        }
    }
}
